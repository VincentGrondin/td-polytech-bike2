#include "Road.h"
#include "Bike.h"

int main()
{
    Road road(20);
    //road.show();
    Bike bike;
    bike.run(road);
    Road Longroad(40);
    bike.run(Longroad);
    return 0;
}
